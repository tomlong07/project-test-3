// Import necessary components from 'react-router-dom'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import React from 'react';
import './App.css';
import HomePage from './pages/homePage/homePage';
import ListAuction from './pages/listAuction/listAuction';
import LoginPage from './pages/authentication/Login/LoginPage';
import Register from './pages/authentication/Register/Register';
import ChangePassword from './pages/authentication/ChangePassword/ChangPassword'
import ViewUserProfile from './pages/UserManagement/ViewUserProfile/ViewUserProfile';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          {/* Define a route for the homepage */}
          <Route path="/" element={<HomePage />} index />
          <Route path="/homepage" element={<HomePage />} index />
          <Route path="/listauction" element={<ListAuction />} index />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<Register />} />
          <Route path="/changpass" element={<ChangePassword />} />

          {/* User Management */}
          <Route path="/user/view" element={<ViewUserProfile />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
