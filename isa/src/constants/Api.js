export const API_URL = "https://vinaauction.onrender.com";

const API = {
    //Authorization
    LOGIN: `${API_URL}/user/login`,
    REGISTER: `${API_URL}/user/signup`,
    CHANGEPASSWORD: `${API_URL}/user/changePassword`,
}

export default API;