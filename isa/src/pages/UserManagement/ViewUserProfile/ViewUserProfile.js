import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import FooterComponent from '../../../components/Footer/FooterComponent';

import axios from "axios";
import API from "../../../constants/Api";


const theme = createTheme();

export default function ViewUserProfile() {

    const navigate = useNavigate();
    const [error, setError] = useState("");
    const [isDisable, setIsDisable] = useState(false);
    const [values, setValues] = useState({
        username: '',
        password: '',
    });

    useEffect(() => {
        let didCancel = false;
        let allInputFieldFilled =
            values.username.trim() !== ""
            && values.password.trim() !== ""
            ;
        setIsDisable(!allInputFieldFilled);
        return () => {
            didCancel = true;
        };
    }, [values]);

    const handleOnSubmit = (e) => {
        e.preventDefault();
        setIsDisable(true);
        axios({
            method: 'POST',
            url: API.LOGIN,
            data: values
        }).then((response) => {
            console.log("RES PONEEE: ", response);
            console.log("JSON: ", JSON.stringify(response.data.data));
            localStorage.setItem('token', JSON.stringify(response.data.data));
            localStorage.setItem('isLoggedIn', '1');
            setIsDisable(false);
            setError("");
            navigate("/");
        }).catch((error) => {
            setIsDisable(false);
            if (error.response.data.error) {
                setError(error.response.data.error);
            } else if (error.message) {
                setError(error.message);
            }
        });
    };


    return (
        <>
            <Container>
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        boxShadow: 3,
                        borderRadius: 2,
                        px: 4,
                        py: 6,
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Log In
                    </Typography>
                    <Box component="form" noValidate sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    value={values.userName}
                                    required
                                    fullWidth
                                    id="userName"
                                    label="User Name"
                                    name="userName"
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    value={values.password}
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="rememberaccount" color="primary" />}
                                    label="Remember this account"
                                />
                            </Grid>
                        </Grid>
                        <Button
                            onClick={handleOnSubmit}
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Submit Edit
                        </Button>
                    </Box>
                </Box>
            </Container>
            <FooterComponent></FooterComponent>
        </>
    );
}