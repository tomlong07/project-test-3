// App.js
import React from 'react';
import FooterComponent from '../../components/Footer/FooterComponent.js';
import AuctionHeader from '../../components/AuctionHeader/AuctionHeader.js';
import SubHeader from '../../components/SubHeader/SubHeader.js';


function listAuction() {
    return (
        <div >
            <SubHeader />
            <AuctionHeader />
            <FooterComponent />
        </div>
    );
}

export default listAuction;