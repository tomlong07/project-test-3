// App.js
import React from 'react';
import IntroductionSection from '../../components/IntroPage/IntroductionSection.js';
import SubintroComponent from '../../components/IntroPage/SubintroComponent.js';
import Header from '../../components/Header/Header.js';
import Body from '../../components/Body/Body.js';
import FooterComponent from '../../components/Footer/FooterComponent.js';


function homePage() {
    return (
        <div >
            <Header />
            <Body />
            <IntroductionSection />
            <SubintroComponent />
            <FooterComponent />
        </div>
    );
}

export default homePage;