import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FooterComponent from '../../../components/Footer/FooterComponent';

import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import axios from "axios";
import API from "../../../constants/Api";

const theme = createTheme();

export default function SignUp() {

    const navigate = useNavigate();
    const [error, setError] = useState("");
    const [isDisable, setIsDisable] = useState(false);
    const [open, setOpen] = useState(false);
    const [values, setValues] = useState({
        old_password: '123123',
        new_password: '12341234',

    });
    const [respone, setRespone] = useState({
        status: '',
        message: '',
    });

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        if (respone.status != "fail") {
            navigate("/");
        } else {
            console.log('sai pass thi chiu');
        }
    };

    useEffect(() => {
        let didCancel = false;
        let allInputFieldFilled =
            values.old_password.trim() !== ""
            && values.new_password.trim() !== ""
            ;
        setIsDisable(!allInputFieldFilled);
        return () => {
            didCancel = true;
        };
    }, [values]);

    // const token = localStorage.getItem('token');
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkVXNlciI6IjY1ZTVlZGZjODBiODdhZjI3MjI0ZWJjYSIsImVtYWlsIjoibmhhdEBnbWFpbC5jb20ifSwiaWF0IjoxNzA5NTY3NDk3fQ.LqYwTQ_2xgFR6nWWfYPemU-OnWpmg4GSftQMmD92QkU';

    const handleInputChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value,
        });
        console.log(e.target.name, ':', e.target.value);
    };

    const onButtonClick = async (e) => {
        e.preventDefault();
        const url = API.CHANGEPASSWORD;
        try {
            // const response = await fetch(url, {
            //     method: "PUT",
            //     data: values,
            //     headers: {
            //         "Content-Type": "application/json",
            //         Authorization: `Bearer ` + token,
            //     },
            //     body: JSON.stringify(values),
            // });
            const response = await fetch(url, {
                method: "PUT",
                data: {
                    
                },
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`,
                },
                body: JSON.stringify(values),
            });

            console.log(error.response);
            console.log(response.data);
            console.log(url);
            console.log(' old', values.old_password + ' new' + values.new_password);
            console.log('Token: ', token);
        } catch (error) {
        }

        // setIsDisable(true);
        // axios.put(API.CHANGEPASSWORD, values, { headers: headers })
        //     .then(function (response) {
        //         const responseData = response.data;
        //         console.log(responseData);
        //         console.log("RES PONEEE: ", response);
        //         setRespone({
        //             ...respone,
        //             status: response.data.status,
        //             message: response.data.message,
        //         })
        //         setIsDisable(false);
        //         setError("");
        //         handleClickOpen();
        //     })
        //     .catch(function (error) {
        //         setIsDisable(false);
        //         if (error.response.data.error) {
        //             console.log('Loi: ', error.response.data.error)
        //             setError(error.response.data.error);
        //         } else if (error.message) {
        //             setError(error.message);
        //         }
        //     });
    };

    return (
        <ThemeProvider theme={theme}>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {respone.status}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {respone.message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
            <Container component="main" maxWidth="sm">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        boxShadow: 3,
                        borderRadius: 2,
                        px: 4,
                        py: 6,
                    }}
                >
                    <Typography component="h1" variant="h5">
                        Change Password
                    </Typography>
                    <Box component="form" noValidate sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="old_password"
                                    label="Old Password"
                                    // type="password"
                                    id="old_password"
                                    // value={values.old_password}
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="new_password"
                                    label="New Password"
                                    // type="password"
                                    id="new_password"
                                    // value={values.new_password}
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="repassword"
                                    label="Re-Password"
                                    // type="password"
                                    id="repassword"
                                    // value={values.repassword}
                                    onChange={handleInputChange}
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            onClick={onButtonClick}
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Submit
                        </Button>

                    </Box>
                </Box>
            </Container>
            <FooterComponent></FooterComponent>
        </ThemeProvider>
    );
}