import React from "react";

const LogoutModal = props => {

    const logoutHandler = () => {
        localStorage.removeItem('isLoggedIn');
        localStorage.removeItem('token');
    };

    return (
        <LogoutModal.Provider
            value={{
                onLogout: logoutHandler,
            }}
        >
            {props.children}
        </LogoutModal.Provider>
    );
}

export default LogoutModal;