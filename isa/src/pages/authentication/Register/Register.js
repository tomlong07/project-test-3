import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FooterComponent from '../../../components/Footer/FooterComponent';

import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import axios from "axios";
import API from "../../../constants/Api";

const theme = createTheme();

export default function SignUp() {

  const navigate = useNavigate();
  const [error, setError] = useState("");
  const [isDisable, setIsDisable] = useState(false);
  const [open, setOpen] = useState(false);
  const [values, setValues] = useState({
    username: '',
    password: '',
    fullName: '',
    email: '',
    identity: '',
    phone: '',
  });
  const [respone, setRespone] = useState({
    status: '',
    message: '',
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    if (respone.status != "fail") {
      navigate("/login");
    } else {
      console.log('Khong dieu huong duoc vi eo dang ky duoc');
    }
  };

  useEffect(() => {
    let didCancel = false;
    let allInputFieldFilled =
      values.username.trim() !== ""
      && values.password.trim() !== ""
      ;
    setIsDisable(!allInputFieldFilled);
    return () => {
      didCancel = true;
    };
  }, [values]);

  const handleInputChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
    console.log(e.target.name, ':', e.target.value);
  };

  const onButtonClick = (e) => {
    e.preventDefault();
    setIsDisable(true);
    axios({
      method: 'POST',
      url: API.REGISTER,
      data: values
    }).then((response) => {
      console.log("RES PONEEE: ", response);
      setRespone({
        ...respone,
        status: response.data.status,
        message: response.data.message,
      })
      setIsDisable(false);
      setError("");
      handleClickOpen();
    }).catch((error) => {
      setIsDisable(false);
      if (error.response.data.error) {
        setError(error.response.data.error);
      } else if (error.message) {
        setError(error.message);
      }
    });
  };

  return (
    <ThemeProvider theme={theme}>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {respone.status}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {respone.message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      <Container component="main" maxWidth="sm">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            boxShadow: 3,
            borderRadius: 2,
            px: 4,
            py: 6,
          }}
        >
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box component="form" noValidate sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="fullName"
                  label="Full Name"
                  id="fullname"
                  value={values.fullName}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="username"
                  label="User Name"
                  id="username"
                  value={values.username}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  value={values.email}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  value={values.password}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="identity"
                  label="Identity Number"
                  id="identity"
                  value={values.identity}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="phone"
                  label="Phone Number"
                  type="phone"
                  id="phone"
                  value={values.phone}
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              onClick={onButtonClick}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link href="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
      <FooterComponent></FooterComponent>
    </ThemeProvider>
  );
}