// SubintroComponent.js

import React from 'react';
import './SubintroComponent.css';

const SubintroComponent = () => {
  // Add the partner images as needed
  const partnerImages = [
    'partner1.jpg',
    'partner2.jpg',
    'partner3.jpg',
    'partner4.jpg',
  ];

  return (
    <div className="subintro-component">
      <div className="subintro-title">Đối tác tiêu biểu</div>
      <div className="partner-images">
        {partnerImages.map((image, index) => (
          <img key={index} src='https://www.apple.com/ac/structured-data/images/knowledge_graph_logo.png?202209082218' alt={`Partner ${index + 1}`} />
        ))}
      </div>

      {/* Ready to participate in the auction section */}
      <div className="auction-cta">
        <div className="cta-title">Sẵn sàng tham gia  <br></br> đấu giá?</div>
        <div className="cta-description">
          Chúng tôi sẵn sàng giúp bạn đáp ứng mọi nhu cầu về đấu giá. <br></br>
          Tiết kiệm thời gian của bạn.
        </div>
        <button className="cta-button" ><a href="#view">Xem sản phẩm đấu giá</a></button>
      </div>
    </div>
  );
};

export default SubintroComponent;
