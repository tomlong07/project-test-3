// IntroductionSection.js

import React, { useState, useEffect } from 'react';
import './IntroductionSection.css';

const IntroductionSection = () => {
  const introductions = [
    { title: 'Title 1', detail: 'The short detail for the first introduce...', link: '#', imageUrl: 'https://yt3.googleusercontent.com/ytc/AIf8zZRU7XvzEaLljpjJjvs_wQhv4klvKspiRyUEA-Nfz68=s900-c-k-c0x00ffffff-no-rj' },
    { title: 'Title 2', detail: 'The short detail for the second introduce...', link: '#', imageUrl: 'https://www.colorcombos.com/images/colors/6D071A.png' },
    { title: 'Title 3', detail: 'The short detail for the second introduce...', link: '#', imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Orange_logo.svg/220px-Orange_logo.svg.png' },
    { title: 'Title 3', detail: 'The short detail for the second introduce...', link: '#', imageUrl: 'https://www.americasfinestlabels.com/includes/work/image_cache/a4cb211cac7697694b91b494f3620ca4.thumb.jpg' },
    { title: 'Title 3', detail: 'The short detail for the second introduce...', link: '#', imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Orange_logo.svg/220px-Orange_logo.svg.png' },


    // Add more introductions as needed
  ];

  const [activeIntroduction, setActiveIntroduction] = useState(0);
  const [isMouseOver, setIsMouseOver] = useState(false);

  // Set the default active introduction to the first one on page load
  useEffect(() => {
    setActiveIntroduction(0);
  }, []);

  // Auto slide to the next introduction every 10 seconds
  useEffect(() => {
    let interval;

    if (!isMouseOver) {
      interval = setInterval(() => {
        setActiveIntroduction((prev) => (prev + 1) % introductions.length);
      }, 3000);
    }

    return () => clearInterval(interval);
  }, [activeIntroduction, introductions.length, isMouseOver]);

  return (
    <div className="introduction-section">
      <div className="section-title">Hoạt động thế nào?</div>
      <div
        className="introduction-container"
        onMouseEnter={() => setIsMouseOver(true)}
        onMouseLeave={() => {
          setIsMouseOver(false);
        }}
      >
        <div className="introduction-left">
          {introductions.map((intro, index) => (
            <div
              key={index}
              className={`introduction ${index === activeIntroduction ? 'active' : ''}`}
              onMouseEnter={() => setActiveIntroduction(index)}
            >
              <div className="intro-title">{intro.title}</div>
              <div className="intro-detail">{intro.detail}</div>
              <a href={intro.link} className="intro-link">More</a>
            </div>
          ))}
        </div>
        <div className="introduction-right">
          {introductions.map((intro, index) => (
            <div key={index} className={`intro-picture ${index === activeIntroduction ? 'active' : ''}`} style={{ width: '700px', height: '700px', objectFit: 'contain' }}>
              <img src={intro.imageUrl} alt={`Illustration for ${intro.title}`} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default IntroductionSection;
