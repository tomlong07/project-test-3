// Header.js
import React from 'react';
import './Header.css';

function SubHeader() {
    return (
        <header className="header">
            {/* Part 1: Top Navigation Bar */}
            <div className="firstHalf">
                <nav className="top-nav">
                    <ul>
                        <li><a href="#help">Giúp đỡ</a></li>
                        <li><a href="#how-to-buy">Làm thế nào để mua</a></li>
                        <li><a href="#how-to-sell">Làm thế nào để bán</a></li>
                        <li><a href="#live-chat">Live Chat</a></li>
                    </ul>
                </nav>

                {/* Part 2: Search Box and Dropdown */}
                <div className="search-container">
                    <div className="part1">
                        <input type="text" placeholder="TÌM KIẾM" className="search-box" />
                        <select className="product-dropdown">
                            <option value="" disabled selected hidden>Loại đấu giá</option>
                            <option value="product1">Product 1</option>
                            <option value="product2">Product 2</option>
                            <option value="product3">Product 3</option>
                            <option value="product4">Product 4</option>
                            <option value="product5">Product 5</option>
                        </select>
                    </div>
                    {/* Part 3: Right Side Links */}
                    <div className="part2">
                        <nav className="right-links">
                            <ul>
                                <li><a href="#login">Đăng nhập</a></li>
                                <li><a href="#signup">Đăng ký</a></li>
                                <li><a href="#get-notification">Thông báo</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                {/* Part 4: Main Navigation Bar */}
                <div className="main-n">
                    <nav className="main-nav">
                        <ul>
                            <li><a href="#home">HOME</a></li>
                            <li><a href="#product1">Product 1</a></li>
                            <li><a href="#product2">Product 2</a></li>
                            <li><a href="#product3">Product 3</a></li>
                            <li><a href="#product3">Product 4</a></li>
                            <li><a href="#product3">Product 5</a></li>
                            <li><a href="#product3">Product 6</a></li>
                            <li><a href="#product3">Product 7</a></li>
                            <li><a href="#product3">Product 8</a></li>
                            <li><a href="#product3">Product 9</a></li>
                            <li><a href="#product3">Product 10</a></li>
                            <select className="product-dropdown1">
                                <option value="" disabled selected hidden>Sản phẩm khác</option>
                                <option value="product1">Product 1</option>
                                <option value="product2">Product 2</option>
                                <option value="product3">Product 3</option>
                                <option value="product4">Product 4</option>
                                <option value="product5">Product 5</option>
                            </select>
                            {/* Add more product links as needed */}
                        </ul>
                    </nav>
                </div>
            </div>
           
        </header >
    );
}

export default SubHeader;
