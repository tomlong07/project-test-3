// FooterComponent.js

import React from 'react';
import './FooterComponent.css';

const FooterComponent = () => {
  return (
    <div className="footer">
      {/* First section with four centered columns */}
      <div className="footer-section">
        <div className="footer-column">
          <img src="logo.png" alt="Logo" />
        </div>
        <div className="footer-column1">
          <div className="column-title">Hỗ trợ</div>
          <div className="column-content">
            <div>Liên hệ với chúng tôi</div>
            <div>Hướng dẫn người mua</div>
            <div>Chính sách bảo mật</div>
            <div>Câu hỏi thường gặp</div>
            <div>Điều khoản sử dụng</div>
          </div>
        </div>
        <div className="footer-column2">
          <div className="column-title">Dành cho người đi đấu giá</div>
          <div className="column-content">
            <div>Làm thế nào để tham gia đấu giá</div>
            <div>Blog cho người tham gia đấu giá</div>
          </div>
        </div>
        <div className="footer-column3">
          <div className="column-title">Dành cho người đấu giá</div>
          <div className="column-content">
            <div>Làm thế nào được phép đăng bài tổ chức đấu giá</div>
            <div>Blog cho người đấu giá</div>
          </div>
        </div>
      </div>

      {/* Bottom section with logos and page name */}
      <div className="footer-bottom">
        <div className="footer-bottom-logos">
          {/* Add three logos as needed */}
          <img src="logo1.png" alt="Logo 1" />
          <img src="logo2.png" alt="Logo 2" />
          <img src="logo3.png" alt="Logo 3" />
        </div>
        <div className="footer-bottom-page-name">ISA - Sàn đấu thầu vật tư công nghiệp</div>
      </div>
    </div>
  );
};

export default FooterComponent;
