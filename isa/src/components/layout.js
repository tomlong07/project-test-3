import React from "react";
import Header from "./Header/Header";
import Footer from "./Footer/FooterComponent";

function MainLayout({ children }) {
    return (
        <>
            <Header />
            {children}
            <Footer />
        </>
    )
}

export default MainLayout;
