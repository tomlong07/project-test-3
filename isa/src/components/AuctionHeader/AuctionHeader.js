// AuctionHeader.js

import React, { useState } from 'react';
import './AuctionHeader.css';
import AuctionItem from '../AuctionItem/AuctionItem';
import Flashcard from '../FlashCard/Flashcard';

const AuctionHeader = () => {
  // Sample data for auction items
  const auctionItems = [
    { id: 1, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },
    { id: 2, name: 'Food & Beverage', /* other properties */ },

    // Add more items as needed
  ];

  // Sample data for flashcards
  const flashcards = [
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },
    { title: "Dây chuyền đóng gói thùng Cartoner dòng ngang 6 và bộ phận thay đổi bao gồm hộp.", imageUrl: "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MT3D3ref_VW_34FR+watch-case-44-aluminum-midnight-nc-se_VW_34FR+watch-face-44-aluminum-midnight-se_VW_34FR?wid=2000&hei=2000&fmt=png-alpha&.v=1694507496485", status: "x", startPrice: "x", date: "x", address: "x" },

    // Add more flashcards as needed
  ];

  const [selectedItemId, setSelectedItemId] = useState(null);

  const handleSelectItem = (itemId) => {
    setSelectedItemId(itemId === selectedItemId ? null : itemId);
  };

  return (
    <div className="auction-header">
      {/* ... existing code ... */}
      <div className="breadcrumb">
        <a href="/">Home</a>
        <span>&gt;</span>
        <a href="/categories">  Food & beverage equipment</a>
      </div>
      <div className="title-and-details">
        <div className="big-title">Food & Beverage<br /> Equipment For Sale</div>
        <div className="details">
          With so many reputable auction houses selling on BidSpotter, it's easy to find food & beverage equipment for sale that will meet your needs. Browse our catalog of current and upcoming auctions to find food storage, kitchen tools & equipment, mixers, ovens, production & processing equipment, and more. Whether you're purchasing to update existing equipment or expand your operation, online catalogs from BidSpotter sellers offer a wide variety of reliable used items at reasonable prices. Use our convenient online auction tools to give yourself the edge in timed online auction events and live online auction events. Add items to your watchlist, set up alerts & reminders, place bids and find great deals on the food & beverage equipment that will work for you.
        </div>
      </div>
      {/* Search Filter Section */}
      <div className="search-filter-section">
        {/* Left Column - Filters */}
        <div className="left-column">
          <div className="item-category">
            <div className='item-category-title'>Loại sản phẩm đấu giá</div>
            {/* Pass auction items data as props to AuctionItem component */}
            {auctionItems.map(item => (
              <AuctionItem
                key={item.id}
                item={item}
                onSelect={() => handleSelectItem(item.id)}
                isSelected={selectedItemId === item.id}
                className="auction-item"
              />
            ))}
            <div>Xem thêm</div>
          </div>

          <div className="location-filter">
            <div>Vị trí sản phẩm</div>
            <select className="adrres-dropdown">
              <option value="" disabled selected hidden>Vị trí</option>
              <option value="address1">Address 1</option>
              <option value="address1">Address 2</option>
              <option value="address1">Address 3</option>
              <option value="address1">Address 4</option>
              <option value="address1">Address 5</option>
            </select>

          </div>
          <button className="search-button">Search</button>

          <div className="status-filter">
            <div>Trạng thái</div>
            <div>Đang đấu giá</div>
            <div>Chờ đấu giá</div>
            <div>Đã kết thúc</div>
          </div>
        </div>

        {/* Right Column - Search Results */}
        <div className="right-column">
          {/* Flashcard Grid */}
          <div className="flashcard-container">
            {/* Render Flashcard components */}
            {flashcards.map((flashcard, index) => (
              <Flashcard key={index} {...flashcard} />
            ))}
          </div>

          {/* Page Pagination */}
          <div className="pagination">Page 1, Page 2, ...</div>
        </div>
      </div>
    </div>
  );
};

export default AuctionHeader;
