import React from 'react';
import './AuctionItem.css';

const AuctionItem = ({ item, onSelect, isSelected }) => {
  return (
    <div
      className={`auction-item ${isSelected ? 'selected' : ''}`}
      onClick={onSelect}
    >
      {item.name}
    </div>
  );
};

export default AuctionItem;
