// Flashcard.js

import React from 'react';
import './Flashcard.css';

const Flashcard = ({ title, imageUrl, status, startPrice, date, address }) => {
  return (
    <div className="flashcard">
      <div className="heart-icon"></div>
      <div className="image-container">
        <img src={imageUrl} alt={`${title} Image`} className="flashcard-image" />
      </div>
      <div className="flashcard-title">{title}</div>
      <div className="flashcard-details">
        <div className="detail-row">
          <div className="detail-label">Status:</div>
          <div className="detail-value">{status}</div>
        </div>
        <div className="detail-row">
          <div className="detail-label">Start Price:</div>
          <div className="detail-value">{startPrice}</div>
        </div>
        <div className="detail-row">
          <div className="detail-label">Date:</div>
          <div className="detail-value">{date}</div>
        </div>
        <div className="detail-row">
          <div className="detail-label">Address:</div>
          <div className="detail-value">{address}</div>
        </div>
      </div>
    </div>
  );
};

export default Flashcard;
