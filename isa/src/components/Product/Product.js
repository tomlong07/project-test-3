import React from 'react';
import './Product.css';

const Product = ({ title, imageUrl, details }) => {
  return (
    <div className="product">
      <div className="product-title">{title}</div>
      <img src={imageUrl} alt={`${title} Image`} />
      <div className="product-details">{details}</div>
    </div>
  );
};

export default Product;
